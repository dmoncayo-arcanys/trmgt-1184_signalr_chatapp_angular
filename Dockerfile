FROM node:12.18.2-alpine AS signalr-chat-angular
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build
### STAGE 2: Run ###
FROM nginx:1.19-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=signalr-chat-angular /usr/src/app/dist/signalr-chat-angular /usr/share/nginx/html