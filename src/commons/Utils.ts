export class Utils {

    public static timepassedInSeconds(date: any = new Date) {
        let current: any = new Date();
        let difference = (current - date) / 1000;
        return difference
    }

    public static formatTimestamp(date: any = new Date) {
        let year = date.getFullYear();
        let month = date.getMonth();
        let day = date.getDate();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];

        return (hours <= 9 ? '0' + hours : hours) + ':' 
            + (minutes <= 9 ? '0' + minutes : minutes) + ' ' + monthNames[month] + ' ' + day + ', ' + year;
    }

}