export class Constants {
    public static TITLE = 'Chat App';
    public static CLIENT_ANGULAR = 'client_angular';
    public static AUTH_SCOPE = 'openid profile email phone SignalRChat.API';
    // public static URL_SERVER = 'https://localhost:44313';
    // public static URL_API = 'https://localhost:44340';
    public static URL_SERVER = 'https://signalrdaleis.azurewebsites.net';
    public static URL_API = 'https://signalrdaleapi.azurewebsites.net';
}