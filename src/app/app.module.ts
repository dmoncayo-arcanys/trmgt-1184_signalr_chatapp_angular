import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AuthModule, LogLevel } from 'angular-auth-oidc-client';

import { ApiService } from '../services/ApiService'
import { AuthService } from '../services/AuthService'
import { Constants } from '../commons/Constants'

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ChatComponent } from './chat/chat.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'chat', component: ChatComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    AuthModule.forRoot({
      config: {
        authority: Constants.URL_SERVER,
        redirectUrl: window.location.origin,
        postLogoutRedirectUri: window.location.origin,
        clientId: Constants.CLIENT_ANGULAR,
        scope: Constants.AUTH_SCOPE,
        responseType: 'code',
        silentRenew: false,
        useRefreshToken: false,
        logLevel: LogLevel.Debug,
      },
    }),
  ],
  providers: [
    ApiService,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
