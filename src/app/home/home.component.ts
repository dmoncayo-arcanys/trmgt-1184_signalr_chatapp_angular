import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/AuthService'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  spinner: boolean = false;
  subscription: any;

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.authService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, idToken }) => {
      if(isAuthenticated) this.router.navigate(['chat']);
    });
    this.subscription = this.route.queryParams.subscribe(params => {
      let code = params.code;
      this.spinner = code !== null && code !== undefined && code !== '';
    });
  }

  ngOnDestroy(): void {
    try { this.subscription.unsubscribe(); } catch(error) {}
  }

  login() {
    this.authService.login();
  }

}
