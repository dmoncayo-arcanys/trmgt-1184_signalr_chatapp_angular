import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/AuthService'
import { ApiService } from '../../services/ApiService';
import { Constants } from '../../commons/Constants'
import { Utils } from '../../commons/Utils'
import * as SignalR from '@microsoft/signalr';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  search: string = '';
  message: string = '';
  interval: any;
  userData: any;
  typingData: any;
  chatList: any = [];
  userList: any = [];
  isConnected: boolean = true;
  
  connection: SignalR.HubConnection = new SignalR.HubConnectionBuilder()  
    .configureLogging(SignalR.LogLevel.Information)  
    .withUrl(Constants.URL_API + '/messaging', { 
      skipNegotiation: true, 
      transport: SignalR.HttpTransportType.WebSockets 
    })  
    .build(); 

  constructor(
    private authService: AuthService, 
    private apiService: ApiService,
    private router: Router) { }

  ngOnInit(): void {
    this.chatList = [];
    this.authService.checkAuth().subscribe(({ isAuthenticated, userData, accessToken, idToken }) => {
      if(!isAuthenticated) this.router.navigate(['']);
      this.userData = userData;
      this.userData.userId = userData.sub;
      this.userData.photo = userData.phone_number;
    });
    this.initOnline();
    this.getUsers();
  }

  ngOnDestroy(): void {
    try {

    } catch(error) {}
  }

  initOnline() {
    let _this = this;
    this.interval = setInterval(function() {
      _this.connection.invoke('online', {
        user: _this.userData,
        isTyping: _this.message.trim() !== '',
        timestamp: Date.parse(new Date() + '')
      });
    }, 500);
  }

  initSignalR() {
    this.connection.start()
      .then(() => { 
        this.isConnected = true;
        console.log('Connected'); 
      })
      .catch((error) => { 
        this.isConnected = false;
        console.log('Error ', error); 
      });

    this.connection.on('send', (data) => {
      data.user = this.getUser(data);
      if(this.typingData !== undefined && data.user.userId === this.typingData.user.userId) {
        this.typingData = undefined;
      }
      if(data.message.trim() !== '') this.chatList.push(data);
      this.scrollIntoView();
    });

    this.connection.on('online', (data) => {
      this.updateOnlineStatus(data);
    });
  }

  getUser(data: any) {
    return this.userList.find((item: any) => item.userId === data.userId); 
  }

  updateOnlineStatus(data: any) {
    let flag = true;
    for(let index = 0; index < this.userList.length; index++) {
      let record = this.userList[index];
      if(data.user.userId === record.userId) {
        record.isOnline = true;
        record.timestamp = data.timestamp;
        flag = false;
      } else if(Utils.timepassedInSeconds(new Date(record.timestamp)) > 3) {
        record.isOnline = false;
      }
    }

    if(data.user.userId !== this.userData.userId && data.isTyping) {
      this.typingData = data;
      this.typing();
    }

    if(flag) {
      this.userList.push({
        userId: data.user.userId,
        photo: data.user.phone_number,
        userName: data.user.name,
        email: data.user.email,
        isOnline: true,
        timestamp: data.timestamp,
      });
    }
  }

  getChat() {
    this.apiService.get('Chat').subscribe((data: any) => {
      if(data.status === 1) {
        for(let index = 0; index < data.response.length; index++) {
          let record = data.response[index];
          record.user = this.getUser(record);
          if(record.message.trim() !== '') this.chatList.push(record);
        }
        this.typing();
      } else {
        this.isConnected = false;
      }
    });
  }

  getUsers() {
    this.userList = [];
    this.apiService.get('Users').subscribe((data: any) => {
      if(data.status === 1) {
        for(let index = 0; index < data.response.length; index++) {
          let record = data.response[index];
          this.userList.push({
            userId: record.id,
            photo: record.photo,
            userName: record.userName,
            email: record.email,
            isOnline: this.userData.userId === record.userId,
            timestamp: null,
          });
        }
        this.initSignalR();
        this.getChat();
      } else {
        this.isConnected = false;
      }
    });
  }

  clickSend() {
    if(this.message.trim() !== '') {
      this.send();
    }
  }

  send() {
    this.apiService.create('chat', {
      message: this.message,
      timestamp: Date.parse(new Date() + ''),
      userId: this.userData.userId
    }).subscribe((data: any) => {
      this.message = '';
      this.typing();
    });
  }

  input() {
    this.typing();
    if(this.message === '') this.send();
  }

  typing() {
    let _this = this;
    setTimeout(function() {
      _this.scrollIntoView();
    }, 10);
  }

  scrollIntoView() {
    let id = window.document.getElementById('chatBox');
    if(id !== null) id.scrollIntoView({ 
      behavior: 'smooth', 
    });
  }

  formatTimestamp(timestamp: number) {
    return Utils.formatTimestamp(new Date(timestamp));
  }

  logout() {
    this.authService.logout();
  }

}
